# riotstrap
Bootstrap components built with Riot.js.

This repository contains a set of native Riot.js components based on Bootstrap's markup and CSS. As a result no dependency on jQuery or Bootstrap's JavaScript is required. The only required dependencies are:

* [Riot.js](http://riotjs.com/) (required ^v2.4.x, test with v2.4.1).
* [Bootstrap CSS](http://getbootstrap.com/) (required 3.x.x, test with 3.3.5).

## Installation

### NPM

```bash
$ npm install riotstrap
```

## Local Setup
  * Install with `npm install`
  * Build with `npm start`.

## Todo
  * button with states
  * tables with sortable columns


## License
  riotstrap is licensed under [The MIT License](LICENSE).

