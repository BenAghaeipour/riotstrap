module.exports =
  isValid: true
  validation: {}
  state: {}

  validate: (form)->
    @isValid = true
    [].slice.call(form?.querySelectorAll('rs-input') || []).map (input)-> input.trigger('validate')
    
  handleValidate: (input)->
    # console.log('validate', input.name, input.validity) if input.validity.valid
    # console.warn('validate', input.name, input.validity, input) if !input.validity.valid
    @updateObj(@validation, input.name, input.validity)
    @isValid = @isValid && input.validity.valid
    @update()

  handleUpdate: (input)->
    # console.log('update', input.name, input.value)   
    @updateObj(@state, input.name, input.value)
    @update()

  updateObj: (obj, key, val)->
    if (/\./g).test(key)
      path = key.match(/(.+)\.(.+)/)
      obj[path[1]] = {} if !obj[path[1]]?
      @updateObj(obj[path[1]],path[2],val)
    else
      obj[key] = val