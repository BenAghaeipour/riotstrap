module.exports =
  getScroll: ()->
    top = window.pageYOffset
    left = window.pageXOffset
    width = window.innerWidth
    height = window.innerHeight

    return {} =
      top: top
      left: left
      bottom: top + height
      right: left + width
      width: width
      height: height

  getOffset: (el)->
    rect = el.getBoundingClientRect()
    clientTop = el.clientTop || document.body.clientTop || 0
    clientLeft = el.clientLeft || document.body.clientLeft || 0
    scroll = @getScroll()
    
    top = rect.top + scroll.top - clientTop
    left = rect.left + scroll.left - clientLeft
    width = el.clientWidth
    height = el.clientHeight

    return {} =
      top: top
      left: left 
      bottom: top + height
      right: left + width
      width: width
      height: height