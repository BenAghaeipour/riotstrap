forms-example
  .form
    rs-text-input(label="Text (rs-text-input)", name="text", value="{state.text}",validation="{validation.text}",onupdate="{handleUpdate}", onvalidate="{handleValidate}",rs-required="{true}")
    rs-text-output(label="Text (rs-text-output)", name="text", value="{state.text}")

    rs-cash-input(label="Cash (rs-cash-input)", name="cash", value="{state.cash}",onupdate="{handleUpdate}", validation="{validation.cash}", onvalidate="{handleValidate}",rs-required="{true}")
    rs-cash-output(label="Cash (rs-cash-output)", name="cash", value="{state.cash}")

    rs-textarea-input(label="Textarea (rs-textarea-input)", name="textarea", value="{state.textarea}",validation="{validation.textarea}", onupdate="{handleUpdate}", onvalidate="{handleValidate}",rs-required="{true}")
    rs-textarea-output(label="Textarea (rs-textarea-output)", name="textarea", value="{state.textarea}")

    rs-checkbox-input(label="Checkbox (rs-checkbox-input)", name="checkbox", value="{state.checkbox}",validation="{validation.checkbox}", onupdate="{handleUpdate}", onvalidate="{handleValidate}",rs-required="{true}")
    rs-checkbox-output(label="Checkbox (rs-checkbox-output)", name="checkbox", value="{state.checkbox}")

    rs-radio-input(label="Radio (rs-radio-input)", name="radio", value="{state.radio}",validation="{validation.radio}", onupdate="{handleUpdate}", onvalidate="{handleValidate}",rs-required="{true}")
    rs-radio-output(label="Radio (rs-radio-output)", name="radio", value="{state.radio}")

    rs-select-input(label="Select (rs-select-input)", name="select", value="{state.select}",validation="{validation.select}", onupdate="{handleUpdate}", onvalidate="{handleValidate}",rs-required="{true}")
      .item(value="one") One
      .item(value="two") Two
      .item(value="three") Three
      .item(value="four") Four
      .item(value="five") Five
      .item(value="six") Six
    rs-select-output(label="Select (rs-select-output)", name="select", value="{state.select}")
      .item(value="one") One
      .item(value="two") Two
      .item(value="three") Three
      .item(value="four") Four
      .item(value="five") Five
      .item(value="six") Six

    rs-radiolist-input(label="Radiolist (rs-radiolist-input)", name="radiolist", value="{state.radiolist}",validation="{validation.radiolist}",onupdate="{handleUpdate}", onvalidate="{handleValidate}",rs-required="{true}",show-other="{true}")
      .item(value="one") One
      .item(value="two") Two
      .item(value="three") Three
      .item(value="four") Four
      .item(value="five") Five
      .item(value="six") Six
    rs-radiolist-output(label="Radiolist (rs-radiolist-output)", name="radiolist", value="{state.radiolist}", other="{state.radiolist_other}")
      .item(value="one") One
      .item(value="two") Two
      .item(value="three") Three
      .item(value="four") Four
      .item(value="five") Five
      .item(value="six") Six
      
    rs-checkboxlist-input(label="Checkboxlist (rs-checkboxlist-input)", name="checkboxlist", value="{state.checkboxlist}",validation="{validation.checkboxlist}",onupdate="{handleUpdate}", onvalidate="{handleValidate}",rs-required="{true}",show-other="{true}")
      .item(value="one") One
      .item(value="two") Two
      .item(value="three") Three
      .item(value="four") Four
      .item(value="five") Five
      .item(value="six") Six
    rs-checkboxlist-output(label="Checkboxlist (rs-checkboxlist-output)", name="checkboxlist", value="{state.checkboxlist}",other="{state.checkboxlist_other}")
      .item(value="one") One
      .item(value="two") Two
      .item(value="three") Three
      .item(value="four") Four
      .item(value="five") Five
      .item(value="six") Six

    rs-file-input(label="File (rs-file-input)", name="file", value="{state.file}",validation="{validation.file}",onupdate="{handleUpdate}", onvalidate="{handleValidate}")
    rs-file-output(label="File (rs-file-output)", name="file", value="{state.file}")

    rs-rating-input(label="Rating (rs-rating-input)", name="rating", value="{state.rating}",validation="{validation.rating}",onupdate="{handleUpdate}", onvalidate="{handleValidate}",rs-required="{true}")    
    rs-rating-output(label="Rating (rs-rating-output)", name="rating", value="{state.rating}")

    rs-button(rs-class="btn-primary",on-click="{submit}") Submit&nbsp;

  script(type="text/coffee").
    @mixin 'forms'

    @on 'mount', ->
      @state = {}
        #text: 'Text'
        #cash: "0.00"
        #textarea: 'Lots of text'
        #checkbox: true
        #radio: false
        #select: 'one'
        #radiolist: ['three']
        #checkboxlist: ['two', 'three']
        #rating: 3
        #file:
        #  name: 'bigfile'
        #  size: 100
        #  type: 'image/png'
        #  url: "https://firebasestorage.googleapis.com/v0/b/project-1294807757591077881.appspot.com/o/account_profile_files%2F-KSBZJIvgzZG7WI7VLP3?alt=media&token=fe212198-7eec-46a7-9437-6c3e831fc1f4"
      @validation = {}
      @update()

    @submit = ()=>
      @validate(@root.querySelector('.form'))
      return Promise.reject() if !@isValid
      
      new Promise (resolve, reject)=>
        setTimeout =>
          return reject() if !@isValid 
          resolve()
        , 2000