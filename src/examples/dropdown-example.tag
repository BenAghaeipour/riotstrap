dropdown-example
  button.btn.btn-success(type="button", onclick="{handleClick}") Toggle Dropdown
  rs-dropdown
    .panel.panel-default
      .panel-heading
        h4.panel-title Dropdown Heading
      .panel-collapse
        .panel-body
          h4 Dropdown Content

  script(type="text/coffee").

    @handleClick = ()->
      @tags['rs-dropdown'].toggle()
      