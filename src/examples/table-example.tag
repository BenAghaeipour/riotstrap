table-example
  rs-table(columns="{mycolumns}", rows="{myrows}", order="{true}", filter="{true}")
    table.table
      thead
        tr
          th(each="{col in columns}")
            rs-order(order="{col.order}" direction="{directions[col.field]}" if="{col.order}") {col.label}
            rs-filter(filter="{col.filter}", with="{col.with}", if="{col.filter}") {col.label}
            div(if="{!col.order && !col.filter}") {col.label}
      tbody
        tr(each="{row in rows}")
          td(each="{col in parent.columns}") 
            {parent.row[col.field]}



  rs-table(name="mytable",rows-name="blah", columns="{mycolumns}", rows="{myrows}", order="{true}", filter="{true}", paginate="{true}")
    table.table.table-striped
      thead
        tr
          th
            rs-order(order="col1 asc", direction="{directions.col1}") Name
          th
            rs-order(order="col2 desc", direction="{directions.col2}") Index
          th
            rs-filter(filter="col3", with="{['4', '23', '29', '30']}") Age
      tbody(show="{blah.length > 0}")
        tr(each="{row in blah}")
          td {row.col1}
          td {row.col2}
          td {row.col3}
      tbody(show="{!blah.length}")
        tr
          td(colspan="3") Loading ...

      tfoot
        tr
          td.text-center(riot-tag="rs-pagination", 
            pages="{tableState.pagination.pages}",
            skip="{tableOptions.pagination.skip}", colspan="4")

  script(type="text/coffee").

    @mycolumns = [
      {label: "Name", field:"col1", order:"col1 asc"}
      {label: "Index", field:"col2", order:"col2 desc"}
      {label: "Age", field:"col3", filter:"col3", with:['4', '23', '29', '30']}
    ]

    @on 'mount', ->
      setTimeout =>
        @myrows = [
          {col1:"Ben", col2:"0", col3:"29"}
          {col1:"Kathryn", col2:"1", col3:"29"}
          {col1:"Max", col2:"2", col3:"23"}
          {col1:"Tilly", col2:"3", col3:"4"}
          {col1:"Sam", col2:"4", col3:"29"}
          {col1:"Tom", col2:"5", col3:"29"}
          {col1:"Jack", col2:"6", col3:"29"}
          {col1:"Diane", col2:"7", col3:"29"}
          {col1:"Colin", col2:"8", col3:"29"}
          {col1:"Rory", col2:"9", col3:"28"}
          {col1:"Nick", col2:"10", col3:"30"}
          {col1:"Dom", col2:"11", col3:"29"}
          {col1:"Mike", col2:"12", col3:"29"}
        ]
        @update()
      , 1000
      setTimeout =>
        @myrows = [
          {col1:"Ben", col2:"0", col3:"29"}
        ]
        @update()
      , 5000