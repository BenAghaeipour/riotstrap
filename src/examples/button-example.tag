button-example

  rs-button(rs-class="btn-primary", on-click="{success}") Successful

  rs-button(rs-class="btn-danger", on-click="{failed}") Failure

  script(type="text/coffee").

    @success = ()->
      new Promise (resolve,reject)=>
        setTimeout =>
          resolve()
        , 1000

    @failed = ()->
      new Promise (resolve,reject)=>
        setTimeout =>
          reject()
        , 1000