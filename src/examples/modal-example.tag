modal-example
  button.btn.btn-primary(onclick="{showModal}") Show Modal
  
  rs-modal
    #{'yield'}(to="header")
      h4 Header
    
    rs-tabs
      rs-tab(heading="Tab 1",active="{true}")
        p Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
      rs-tab(heading="Tab 2")
        p Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
      rs-tab(heading="Tab 3")
        p Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
      rs-tab(heading="Tab 4")
        p Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
    
    #{'yield'}(to="footer")
      .btn-group
        button.btn.btn-primary(onclick="{close}") Close

  script(type="text/coffee").
    
    @showModal = ()->
      @tags['rs-modal'].open()