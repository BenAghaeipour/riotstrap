alerts-example
  rs-alert(type="info")
    h1 Hello


  .notification
    rs-alert(type="success", onclose="{close}")
      span Success
    rs-alert(type="info", timeout="1000")
      span Info
    rs-alert(type="warning")
      span Warning
    rs-alert(type="danger")
      span Danger

  style(type="text/stylus").
    .notification 
      position: fixed
      z-index: 9999
      top: 12px
      right: 12px
      width: 333px

  script(type="text/coffee").
    @close = ()=>
  