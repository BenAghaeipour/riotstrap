rs-input
  div(if="{inputType == 'input'}")
    input(name="{opts.name}", 
      type="{type}",
      class="{opts.rsClass}",
      placeholder="{opts.placeholder}",
      oninput="{handleChange}",
      onblur="{handleBlur}",
      value="{opts.value}",
      required="{opts.rsRequired}",
      pattern="{opts.pattern}",
      disabled="{opts.rsDisabled}")
        
  div(if="{inputType == 'textarea'}")
    textarea(name="{opts.name}",
      class="{opts.rsClass}",
      placeholder="{opts.placeholder}",
      oninput="{handleChange}",
      value="{opts.value}",
      rows="{opts.rsRows}",
      required="{opts.rsRequired}")
  
  div(if="{inputType == 'select'}")
    select(name="{opts.name}", 
      onchange="{handleChange}",
      value="{opts.value}",
      class="{opts.rsClass}", 
      required="{opts.rsRequired}")
      option(value="") Please Select
      option(value="{option.value}", each="{option in items}", selected="{parent.opts.value == option.value}") {option.text}

  div(if="{inputType == 'checkbox' || inputType == 'radio'}")
    input(name="{opts.name}",
      type="{type}",
      class="{opts.rsClass}",
      onchange="{handleChange}",
      checked="{opts.value}",
      required="{opts.rsRequired}")

  div(if="{inputType == 'radiolist'}")
    .radiolist
      .radio(each="{radio in items}")
        label
          input(type="radio", name="{parent.opts.name}", value="{radio.value}", checked="{parent.opts.value.indexOf(radio.value) > -1}", onchange="{parent.handleCheckedChange}")
          | {radio.text}
      .radio(if="{opts.showOther}")
        label
          input(type="radio", name="{opts.name}", value="other", checked="{opts.value.indexOf('other') > -1}", onchange="{handleCheckedChange}")
          | Other
      .form-group
        label.control-label(if="{opts.value.indexOf('other') > -1}") Please Specify
        input.form-control(name="{opts.name}_other", type="text", oninput="{handleChange}", value="{opts.other}", if="{opts.value.indexOf('other') > -1}")      
  
  div(if="{inputType == 'checkboxlist'}")
    .rs-input-checkboxlist
      .checkbox(each="{checkbox in items}")
        label
          input(type="checkbox", name="{parent.opts.name}", value="{checkbox.value}", checked="{parent.opts.value.indexOf(checkbox.value) > -1}", onchange="{parent.handleCheckedChange}")
          | {checkbox.text}
      .checkbox(if="{opts.showOther}")
        label
          input(type="checkbox", name="{opts.name}", value="other", checked="{opts.value.indexOf('other') > -1}", onchange="{handleCheckedChange}")
          | Other
      .form-group
        label.control-label(if="{opts.value.indexOf('other') > -1}") Please Specify
        input.form-control(name="{opts.name}_other", type="text", oninput="{handleChange}", value="{opts.other}", if="{opts.value.indexOf('other') > -1}")      
  
  div(if="{inputType == 'file'}")
    .rs-input-file
      .rs-input-file-upload(onclick="{pickFile}")
        i.fa.fa-4x.fa-cloud-upload(if="{!uploading}")
        i.fa.fa-spinner.fa-pulse.fa-3x.fa-fw(if="{uploading}")
      input(name="{opts.name}",
        type="file", 
        class="{opts.rsClass}",
        onchange="{handleChange}",
        required="{opts.rsRequired}")
      .rs-input-file-progress
        {opts.progress}

  div(if="{inputType == 'rating'}")
    .rs-input-rating
      i.fa.fa-2x.fa-star-o(each="{v,rating in new Array(opts.outOf || 5)}", class="{active: parent.opts.value > rating}", onclick="{parent.handleChange}")

  div.rs-input-items
    <yield/>

  style(type="text/stylus").
    rs-input
      .rs-input-rating
        display: inline-block
        &:hover
          i
            &:before
              content:"\f005"
              color: gold

            &:hover ~ i:before
              content:"\f006"
              color: black
        i
          &.active
            &:before
              content:"\f005"
              color: gold
            
      .rs-input-file
        display: inline-block
        
        div
          padding: 20px
        input
          display: none
          
      .rs-input-items
        display: none

  script(type="text/coffee").
    @on 'mount', ->
      @type = opts.type || "text"
      @items = opts.items || []
      @root.trigger = @trigger

      switch @type
        when 'textarea' then @inputType = 'textarea'
        when 'file' then @inputType = 'file'
        when 'select' then @inputType = 'select'
        when 'checkbox' then @inputType = 'checkbox'
        when 'radio' then @inputType = 'radio'
        when 'radiolist' then @inputType = 'radiolist'
        when 'checkboxlist' then @inputType = 'checkboxlist'
        when 'rating' then @inputType = 'rating'
        else @inputType = 'input'
     
      @items = @getItems() if (@type == 'select' || @type == 'radiolist' || @type == 'checkboxlist') && !opts.options?
      @update()

    @on 'update', =>
      if @inputType == 'file'
        @uploading = opts.progress?
      if @inputType == 'input' and @type == 'text' and @start? and @end?
        input = @root.querySelector('input[name="' + @opts.name + '"]')
        input.setSelectionRange(@start, @end) if input? && input == document.activeElement
        
    @pickFile = ()=>
       @root.querySelector('input').click()
     
    @handleBlur = (val)=>
      name = val.target.name || opts.name
      value = @getValue(val)
      @opts.onblur({name, value}) if opts.onblur?

    @getValue = (val)=>
      value = val.target.value
      value = val.target.checked if @inputType == 'checkbox' or @inputType == 'radio'
      value = val.target.files[0] if @inputType == 'file'
      value = val.item.rating + 1 if @inputType == 'rating'
      return value

    @handleChange = (val)=>
      name = val.target.name || opts.name
      value = @getValue(val)
      if @inputType == 'input' and @type == 'text'
        @start = val.target.selectionStart
        @end = val.target.selectionEnd
      @opts.onupdate({name, value}) if opts.onupdate?
      @trigger('validate') if @validateOnChange
      
    @handleCheckedChange = (val)=>      
      value = ([].slice.call(@root.querySelectorAll('input[type="radio"], input[type="checkbox"]') || []).filter (input)-> input.checked).map (input)-> input.value
      @opts.onupdate({name:opts.name, value}) if @opts.onupdate?
      @trigger('validate') if @validateOnChange

    @on 'validate', (continueValidating=true)=>
      @validateOnChange = continueValidating
      
      if @inputType == 'checkboxlist' || @inputType == 'radiolist'
        @validateList()
      else if @inputType == 'rating'
        @validateRating()
      else
        @validate()

    @validate = ()->
      input = @root.querySelector('input, select, textarea')
      @opts.onvalidate({name:opts.name, validity:input.validity}) if @opts.onvalidate? && input?.validity?
    
    @validateRating = ()=>
      validity = 
        valueMissing: false

      if opts.rsRequired
        validity.valueMissing = !(opts.value > 0)

      validity.valid = validityIsValid(validity)
      @opts.onvalidate({name:opts.name, validity}) if @opts.onvalidate?
      
    @validateList = ()=>
      validity = 
        valueMissing: false

      if opts.rsRequired
        validity.valueMissing = true 
        inputs = [].slice.call @root.querySelectorAll('input[type="checkbox"], input[type="radio"]') 
        if inputs.length == 0
          validity.valueMissing = false 
        else
          inputs.map (input)->
            validity.valueMissing = false if input.checked

      validity.valid = validityIsValid(validity)
      @opts.onvalidate({name:opts.name, validity}) if @opts.onvalidate?

    validityIsValid = (validity)->
      !validity.badInput && !validity.customError && !validity.patternMismatch && !validity.rangeOverflow && !validity.rangeUnderflow &&
      !validity.stepMismatch && !validity.tooLong && !validity.tooShort && !validity.typeMismatch && !validity.valueMissing

    @getItems = ->
      [].slice.call(@root.querySelector('.rs-input-items')?.querySelectorAll('.item') || []).map (item)->
        ret = text: item.innerHTML
        [].slice.call(item.attributes).map (itm)-> 
          ret[itm.name] = itm.value
        return ret
    
rs-output
  div(if="{inputType == 'input' || inputType == 'textarea' || inputType == 'checkbox' || inputType == 'radio'}") 
    p(class="{opts.rsClass}") {opts.value} 

  div(if="{inputType == 'select' || inputType == 'radiolist' || inputType == 'checkboxlist'}")
    p(each="{item in items}", if="{parent.findMatch(item.value)}") {item.text}
    p(if="{opts.value.indexOf('other')> -1}") {opts.other}

  div(if="{inputType == 'other'}")
    .rs-output-other
      span(each="{item in items}", if="{parent.findMatch(item.value)}") {item.text}
      span(if="{opts.value.indexOf('other')> -1}") {opts.other}
    
  div(if="{inputType == 'file'}")
    .rs-output-file(if="{opts.value}")
      .filetype
        i.fa(class="{'fa-' + icon + '-o fa-' + (opts.iconSize || '4') + 'x'}", if="{!opts.hideIcon}")
      .filename(class="{longname:opts.value.name.length > 15}") 
        span {opts.value.name}
      .filesize
        span ({humanFileSize(opts.value.size)})
      .remove(onclick="{opts.remove}" if="{opts.remove && opts.value}")
        i.fa.fa-trash-o.fa-2x
      a.download(href="{opts.value.url}" if="{opts.value.url}", download="newfile")
        i.fa.fa-cloud-download.fa-2x

  div(if="{inputType == 'rating'}")
    .rs-output-rating
      i.fa.fa-2x.fa-star-o(each="{v,rating in new Array(opts.outOf || 5)}", class="{active: parent.opts.value > rating}")

  .rs-output-items
    <yield/>

  style(type="text/stylus").
    rs-output
      .rs-output-other
        span
          margin-right: 5px
      .rs-output-rating
        display: inline-block
        i
          &.active
            &:before
              content:"\f005"
              color: gold
            
      .rs-output-file
        display: inline-block
        padding: 25px
        position: relative
        
        &:hover
          .remove, .download 
            opacity: 1

        .remove, .download
          transition: all 1s ease
          display: block
          position: absolute
          top: 0
          cursor: pointer
          opacity: 0
        .remove
          left: 0
        .download
          right: 0
        
        .filename
          position: absolute
          bottom: 0
          left: 0
          right: 0
          overflow: hidden
          text-align: center

          &.longname
            overflow: hidden
            text-align: left
            span
              white-space: nowrap
              text-overflow: ellipsis
              
              position: relative
              right: 0
              
              transition: all 3s ease
              &:hover
                right: 200%
      .rs-output-items
        display: none

  script(type="text/coffee").
    @on 'mount', ->
      @type = opts.type || "text"
      @items = opts.items || []
      @root.trigger = @trigger

      switch @type
        when 'textarea' then @inputType = 'textarea'
        when 'file' then @inputType = 'file'
        when 'select' then @inputType = 'select'
        when 'checkbox' then @inputType = 'checkbox'
        when 'radio' then @inputType = 'radio'
        when 'radiolist' then @inputType = 'radiolist'
        when 'checkboxlist' then @inputType = 'checkboxlist'
        when 'filelist' then @inputType = 'filelist'
        when 'rating' then @inputType = 'rating'
        when 'other' then @inputType = 'other'
        else @inputType = 'input'
     
      @items = @getItems() if (@type == 'select' || @type == 'radiolist' || @type == 'checkboxlist' || @type == 'other') && !opts.options?
      @update()

    @on 'update', ->
      if @inputType == 'file'
        @icon = switch opts?.value?.type
          when 'image/png' then 'file-image'
          when 'application/vnd.openxmlformats-officedocument.presentationml.presentation' then 'file-powerpoint'
          when 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' then 'file-excel'
          when 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' then 'file-word'
          when 'application/pdf' then 'file-pdf'
          when 'text/plain' then 'file-text'
          else 'file'

    @getItems = ->
      [].slice.call(@root.querySelector('.rs-output-items')?.querySelectorAll('.item') || []).map (item)->
        ret = text: item.innerHTML
        [].slice.call(item.attributes).map (itm)-> 
          ret[itm.name] = itm.value
        return ret
    
    @findMatch = (itemValue)->
      if Array.isArray(opts.value) then opts.value.indexOf(itemValue) > -1 else new RegExp("^"+opts.value+"$", "g").test(itemValue)

    @humanFileSize = (bytes, si) ->
      return bytes + ' B' if Math.abs(bytes) < 1000
      units = ['kB', 'MB', 'GB']
      u = -1
      loop
        bytes /= 1000
        ++u
        unless Math.abs(bytes) >= 1000 and u < units.length - 1
          break
      bytes.toFixed(1) + ' ' + units[u]