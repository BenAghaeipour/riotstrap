rs-affix
  .rs-affix(style="{styles}",class="{'fixed':fixed}")
    <yield/>

  style(type="text/stylus").
    .rs-affix
      &.fixed
        position: fixed

  script(type="text/coffee").
    @utils = require '../shared/utils.coffee'
    @offset = 0
    @fixed = false
    @styles = null

    @on 'mount', ->
      @fixed = opts.fixed if opts.fixed?
      window.addEventListener('scroll', scrolling)
      window.addEventListener('resize', scrolling)

    @on 'unmount', ->
      window.removeEventListener('scroll', scrolling)
      window.removeEventListener('resize', scrolling)

    scrolling = ()=>
      scroll = @utils.getScroll()
      elementOffset = @utils.getOffset(@root)
      if !@fixed && scroll.top > elementOffset.top
        @fixed = if opts.fixed? then opts.fixed else true
        @styles = 'top: ' + @offset + 'px; left: ' + elementOffset.left + 'px; width: ' + @root.offsetWidth + 'px;'
      if @fixed && scroll.top < elementOffset.top
        @fixed = false
        @styles = null
      @update()