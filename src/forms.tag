rs-text-input
  .form-group(class="{'has-error':!opts.validation.valid}")
    label.control-label {opts.label}
    rs-input(name="{opts.name}",
      type="{opts.type}"
      onupdate="{opts.onupdate}",
      onvalidate="{opts.onvalidate}",
      value="{opts.value}",
      rs-required="{opts.rsRequired}",
      rs-disabled="{opts.rsDisabled}",
      rs-class="form-control",
      placeholder="{opts.placeholder}")

    .help-block(if="{opts.validation.valueMissing}") This field is required

rs-text-output
  .form-group
    label.control-label {opts.label}
    rs-output(name="{opts.name}", value="{opts.value}",rs-class="form-control-static")
  
rs-cash-input
  .form-group(class="{'has-error':!opts.validation.valid}")
    label.control-label {opts.label}
    .input-group
      .input-group-addon &pound;
      rs-input(name="{opts.name}",
        type="{opts.type}"
        onupdate="{opts.onupdate}",
        onvalidate="{opts.onvalidate}",
        onblur="{toFixed}",
        value="{opts.value}",
        rs-required="{opts.rsRequired}",
        rs-class="form-control")
      
    .help-block(if="{opts.validation.valueMissing}") This field is required

  script(type="text/coffee").

    @toFixed = (val)->
      value = +val.value
      val.value = "" if !val.value?
      val.value = "" if isNaN(val.value)  
      val.value = value.toFixed(2) if val.value != ""
      opts.onupdate(val) if opts.onupdate?
  
rs-cash-output
  .form-group
    label.control-label {opts.label}
    rs-output(name="{opts.name}", value="{value}",rs-class="form-control-static")
    
  script(type="text/coffee").
    @on 'update', ->
      @value = @toFixed(opts.value) || ""
    
    @toFixed = (val)->
      value = +val
      return "" if !value?
      return "" if isNaN(value) 
      return "£" + value.toFixed(2) if val != ""

rs-textarea-input
  .form-group(class="{'has-error':!opts.validation.valid}")
    label.control-label {opts.label}
    rs-input(name="{opts.name}",
      type="textarea",
      onupdate="{opts.onupdate}",
      onvalidate="{opts.onvalidate}",
      value="{opts.value}",
      rs-required="{opts.rsRequired}",
      rs-rows="5"
      rs-class="form-control") 
    .help-block(if="{opts.validation.valueMissing}") This field is required  

rs-textarea-output
  .form-group
    label.control-label {opts.label}
    rs-output(name="{opts.name}",type="textarea",value="{opts.value}",rs-rows="5", rs-class="form-control-static")         

rs-checkbox-input
  div(class="{'has-error':!opts.validation.valid}")
    .checkbox
      label
        rs-input(name="{opts.name}",
          type="checkbox",
          onupdate="{opts.onupdate}",
          onvalidate="{opts.onvalidate}",
          value="{opts.value}",
          rs-required="{opts.rsRequired}")
        | {opts.label}
    .help-block(if="{opts.validation.valueMissing}") This field is required        

rs-checkbox-output
  .form-group
    label {opts.label}
    rs-output(name="{opts.name}", type="checkbox", value="{opts.value ? 'True' : 'False'}", rs-class="form-control-static")

rs-radio-input
  div(class="{'has-error':!opts.validation.valid}")
    .radio
      label      
        rs-input(name="{opts.name}",
          type="radio",
          onupdate="{opts.onupdate}",
          onvalidate="{opts.onvalidate}",
          value="{opts.value}",
          rs-required="{opts.rsRequired}")
        | {opts.label}
    .help-block(if="{opts.validation.valueMissing}") This field is required        
  
rs-radio-output
  .form-group
    label {opts.label}
    rs-output(name="{opts.name}", type="radio", value="{opts.value ? 'True' : 'False'}", rs-class="form-control-static")

rs-select-input
  .form-group(class="{'has-error':!opts.validation.valid}")
    label.control-label {opts.label}      
    rs-input(name="{opts.name}",
      type="select",
      onupdate="{opts.onupdate}",
      onvalidate="{opts.onvalidate}",
      value="{opts.value}",
      rs-required="{opts.rsRequired}",
      rs-class="form-control")
      <yield/>
               
    .help-block(if="{opts.validation.valueMissing}") This field is required       
  
rs-select-output
  .form-group
    label.control-label {opts.label}      
    rs-output(name="{opts.name}", type="select", value="{opts.value}",rs-class="form-control-static")
      <yield/>
             
rs-radiolist-input
  .form-group(class="{'has-error':!opts.validation.valid}")
    label.control-label {opts.label}      
    rs-input(name="{opts.name}",
      type="radiolist",
      onupdate="{opts.onupdate}",
      onvalidate="{opts.onvalidate}",
      value="{opts.value}",
      rs-required="{opts.rsRequired}",
      show-other="{opts.showOther}",
      show-other-input="{opts.value.indexOf('other') > -1}",
      other="{opts.other}")
      <yield/>
    .help-block(if="{opts.validation.valueMissing}") This field is required
  
  style(type="text/stylus").
    rs-radiolist-input
      .radio 
        display: inline-block
        width: 50%

rs-radiolist-output
  .form-group
    label.control-label {opts.label}          
    rs-output(name="{opts.name}", type="radiolist", value="{opts.value}", other="{opts.other}")
      <yield/>
      
rs-checkboxlist-input
  .form-group(class="{'has-error':!opts.validation.valid}")
    label.control-label {opts.label}      
    rs-input(name="{opts.name}",
      type="checkboxlist",
      onupdate="{opts.onupdate}",
      onvalidate="{opts.onvalidate}",
      value="{opts.value}",        
      rs-required="{opts.rsRequired}",
      show-other="{opts.showOther}",
      show-other-input="{opts.value.indexOf('other') > -1}",
      other="{opts.other}")
      <yield/>
    .help-block(if="{opts.validation.valueMissing}") This field is required
  
  style(type="text/stylus").
    rs-checkboxlist-input
      .checkbox 
        display: inline-block
        width: 50%

rs-checkboxlist-output
  .form-group
    label.control-label {opts.label}
    rs-output(name="{opts.name}", type="checkboxlist", value="{opts.value}", other="{opts.other}")
      <yield/>

rs-file-input
  .form-group(class="{'has-error':!opts.validation.valid}")
    label.control-label {opts.label}
    rs-input(name="{opts.name}",
      type="file",
      onupdate="{opts.onupdate}",
      onvalidate="{opts.onvalidate}",
      rs-required="{opts.rsRequired}",
      progress="{progress}")
    .help-block(if="{opts.validation.valueMissing}") This field is required

rs-file-output
  .form-group
    label.control-label {opts.label}
    rs-output(name="{opts.name}",type="file",value="{opts.value}",rs-class="form-control-static")

rs-files-output
  .form-group
    label.control-label {opts.label}
    .files
      rs-output(name="{parent.opts.value[file]._key}",type="file",value="{parent.opts.value[file]}",rs-class="form-control-static",each="{file in Object.keys(opts.value)}")

  style(type="text/stylus").
    .files
      rs-output
        display: inline-block
  
rs-rating-input
  .form-group(class="{'has-error':!opts.validation.valid}")
    label.control-label {opts.label}
    rs-input(name="{opts.name}",
      type="rating",
      onupdate="{opts.onupdate}",
      onvalidate="{opts.onvalidate}",
      value="{opts.value}",
      rs-required="{opts.rsRequired}",
      rs-class="form-control",
      out-of="{5}")
    .help-block(if="{opts.validation.valueMissing}") This field is required
  
rs-rating-output
  .form-group
    label.control-label {opts.label}
    rs-output(name="{opts.name}", type="rating", value="{opts.value}",out-of="{5}")

rs-other-output
  rs-output(name="{opts.name}", type="other", value="{opts.value}", other="{opts.other}")
    <yield/>

// form-recaptcha
//   .form-group
//     recaptcha(name="{opts.name}", onupdate="{opts.onupdate}")

//   script(type="text/coffee").
//     @reset = =>
//       @tags.recaptcha.reset()