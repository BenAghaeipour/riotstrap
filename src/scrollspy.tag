rs-scrollspy
  <yield />

  script(type="text/coffee").
    @utils = require '../shared/utils.coffee'

    @targets = []
    @anchors = []

    @on 'mount', ->
      @anchors = [].slice.call @root.querySelectorAll('[scrollspy-link]')
      @targets = @anchors.map (a, i)->
        target = (a.attributes.to.value).match(/(.*)$/g)?[0]
        return document.querySelector(target) if target?
      window.addEventListener('scroll', @handleSpy)     
      @handleSpy()

    @on 'unmount', ->
      window.removeEventListener('scroll', @handleSpy)

    @handleSpy = ()=>
      if @timer?
        clearTimeout(@timer)
        @timer = null
      @timer = setTimeout (=> @spy()), 10

    @spy = ()->
      foundInView = false
      scrollspied = {}
      @targets.map (target, i)=>
        inView = @isElementInView(target)
        scrollspied[i] = if !foundInView then inView else false
        foundInView = true if inView
      @scrollspied = scrollspied
      @update()

    @isElementInView = (el)->
      scroll = @utils.getScroll()
      elementOffset = @utils.getOffset(el)
      return (elementOffset.top < scroll.bottom) && (elementOffset.bottom > scroll.top)