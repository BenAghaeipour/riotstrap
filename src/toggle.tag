rs-toggle
  <yield/>

  style(type="text/stylus").
    .toggle
      display: block
      overflow: hidden
      
      &.slide, &.fade
        transition: all 0.5s linear
        opacity: 0
        max-height: 0px
        
        &.open
          max-height: 800px 
          opacity: 1

  script(type="text/coffee").

    @on 'mount', ->
      @hidden = true
      @style = opts.toggleStyle?.split(',') || ['slide']
      @style.push 'toggle'
      @style.map (style)->
        document.querySelector(opts.toggle).classList.add(style)
      @open() if opts.start == 'open'
      @root.addEventListener('click', @toggle)

    @on 'unmount', ->
      @root.removeEventListener('click', @toggle)
      
    @toggle = =>
      if @hidden then @open() else @close() 

    @open = =>
      @hidden = false
      document.querySelector(opts.toggle).classList.add('open')
      @update()

    @close = =>
      @hidden = true
      document.querySelector(opts.toggle).classList.remove('open')
      @update()      