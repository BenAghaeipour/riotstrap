riot = require 'riot'

require '../accordion.tag'
require '../affix.tag'
require '../alert.tag'
require '../aside.tag'
require '../buttons.tag'
require '../carousel.tag'
require '../datepicker.tag'
require '../dropdown.tag'
require '../forms.tag'
require '../inputs.tag'
require '../modal.tag'
require '../panel.tag'
require '../popover.tag'
require '../progressbar.tag'
require '../scrollspy.tag'
require '../select.tag'
require '../smoothscroll.tag'
require '../spinner.tag'
require '../table.tag'
require '../tabs.tag'
require '../toggle.tag'
require '../tooltip.tag'
require '../typeahead.tag'

require '../raw.tag'

require '../examples/accordion-example.tag'
require '../examples/alerts-example.tag'
require '../examples/button-example.tag'
require '../examples/dropdown-example.tag'
require '../examples/forms-example.tag'
require '../examples/modal-example.tag'
require '../examples/table-example.tag'
require '../examples/tabs-example.tag'
require '../examples/toggle-example.tag'
require '../examples/smoothscroll-example.tag'

headers = ([].slice.call document.querySelectorAll('h3.anchor')).map (header)-> header.innerHTML


riot.mixin('forms', require '../../shared/mixins/forms.coffee')
riot.mount('*', {headers})