rs-table
  .rs-table
    <yield />
      
  script(type="text/coffee").
    firstBy = require 'thenby'

    @on 'mount', ->
      @_rows = []
      @tableName = opts.tableName || 'table'
      @rowsName = opts.rowsName || 'rows'
      @columnsName = opts.columnsName || 'columns'
      #console.log('mounted table', @rowsName, @columnsName)
      @orders = []
      @filters = []
      @directions = {}

      @tableOptions =
        options: []
        filters: []
        pagination: 
          skip: opts.page || 0
          take: +opts.perPage || 5

      @tableState =
        orders: {}
        filters: {}
        pagination:
          pages:0

    @on 'update', ->
      if @tableName
        if @_rows != opts.rows
          @tableOptions.pagination.skip = 0
          @_rows = opts.rows
        
        #console.log('update shiz')

        #console.log('updated table',@rowsName, @columnsName)
        @[@rowsName] = opts.rows || []
        @[@columnsName] = opts.columns || []
        @[@rowsName] = @filterBy(@[@rowsName], @filters) if opts.filter
        @orderBy(@[@rowsName], @orders) if opts.order
        @[@rowsName] = @paginateBy(@[@rowsName]) if opts.paginate

    @setIgnoreCase = (obj)=>
      obj.opts = {} if !obj.opts?
      obj.opts.ignoreCase = true if !obj.opts.ignoreCase? 
      obj

    @objectifyStrings = (str)-> if toString.call(str) == '[object String]' then {key: str} else str
      
    @paginateBy = (objs)=>
      #console.log('paginate by ', @tableOptions.pagination)
      @tableState.pagination.pages = Math.ceil(objs.length / @tableOptions.pagination.take)
      skip = @tableOptions.pagination.skip * @tableOptions.pagination.take
      take = skip + @tableOptions.pagination.take
      objs.slice(skip, take)

    @orderBy = (objs, orders)=>
      return objs if !orders?
      orders = {key: orders} if toString.call(orders) == '[object String]'
      orders = [orders] if !Array.isArray(orders)
      orders = orders.map(@objectifyStrings)
      orders = orders.map(@setIgnoreCase)
      firstOrder = orders.splice(0, 1)[0] if orders.length > 0
      return objs if !firstOrder?
      objs.sort(@extraOrders(firstBy(firstOrder.key, firstOrder.opts), orders))
    
    @extraOrders = (ordered, orders=[])=>
      orders.map (o)->
        o = {key: o} if toString.call(o) == '[object String]'
        ordered = ordered.thenBy(o.key, o.opts)
      ordered

    @filterBy = (objs, filters)=>
      return objs if !filters?
      for filterKey, filterValue of filters
        if filterValue?
          objs = objs.filter (obj)-> obj[filterKey] == filterValue      
      objs

    @order = (key, direction=1)=>
      #console.log('order')
      if !@directions[key]? || @directions[key] == 0
        @directions[key] = direction
      else if @directions[key] == direction
        @directions[key] = -direction
      else 
        @directions[key] = 0

      @orders = @orders.filter (order)=>
        @directions[order.key]? and @directions[order.key] != 0 and order.key != key
      @orders.push({key: key, opts: direction: @directions[key]})
      @update()

    @filter = (key, value)=>
      #console.log('filter')
      @filters[key] = value
      @update()
    
    @paginate = ({skip})=>
      @tableOptions.pagination.skip = skip if skip?
      @update()

rs-filter
  .rs-filter(onclick="{handleClick}")
    <yield/>
    .fa.filter-applied.fa-filter(if="{by}")

  script(type="text/coffee").
    @on 'mount', ->
      @table = opts.table or @
      @table = @table.parent while(@table? and (@table.root?.tagName != 'RS-TABLE' and @table.opts?.riotTag != 'rs-table'))
      console.warn('rs-filter must be used with an rs-table') if !@table?

    @cycleWith = ()=> 
      @with = opts.with
      console.warn('rs-filter must have a list to filter with') if !@with?
      return @with[0] if !@by?
      index = @with.indexOf(@by)
      return null if index == -1 
      return @with[index+1]

    @handleClick = (e)=>
      e.preventUpdate = true
      
      #console.log('handle rs-filter', e)
      return if !opts.filter? 
      @by = @cycleWith()
      @table.filter(opts.filter,@by) if @table?
      @update()
      
  style(type="text/stylus").
    .rs-filter
      cursor: pointer
      position: relative
      
      .filter-applied
        position: absolute
        top: 25%
        right: 10px
rs-order
  .rs-order(onclick="{handleClick}")
    <yield/>
    .fa.order-direction(class="{'fa-caret-up':opts.direction==1,'fa-caret-down':opts.direction==-1}")

  style(type="text/stylus").
    .rs-order
      cursor: pointer
      position: relative
      
      .order-direction
        position: absolute
        top: 25%
        right: 10px

  script(type="text/coffee").
    @on 'mount', ->
      @table = opts.table or @
      @table = @table.parent while(@table? and (@table.root?.tagName != 'RS-TABLE' and @table.opts?.riotTag != 'rs-table'))
      console.warn('rs-order must be used with an rs-table') if !@table?
      
    @handleClick = (e)=>
      e.preventUpdate = true      
      #console.log('handle rs-order')
      return if !opts.order?
      order = opts.order?.split(' ')
      field = order[0]       
      direction = if order[1] == 'desc' then -1 else if order[1] == 'asc' then 1 else 0
      @table.order(field, direction) if @table? and field?

rs-pagination
  .rs-pagination
    ul.pagination
      li(class="{disabled:skip <= 0}")
        span.fa.fa-chevron-left(onclick="{prev}")
      li(each="{page in pages}")
        span(onclick="{parent.goto.bind(null, page)}") page
      li(class="{disabled:skip >= pages - 1}")
        span.fa.fa-chevron-right(onclick="{next}")
      
  style(type="text/stylus").
    .rs-pagination
      ul
        li
          span
            cursor: pointer

  script(type="text/coffee").
    @on 'mount', ->
      @table = opts.table or @
      @table = @table.parent while(@table? and (@table.root?.tagName != 'RS-TABLE' and @table.opts?.riotTag != 'rs-table'))
      console.warn('rs-pagination must be used with an rs-table') if !@table?
    
    @on 'update', ->
      @pages = opts.pages
      @skip = opts.skip
      #console.log('update pagination', @skip, @pages)
      
    @prev = (e)=>
      e.preventUpdate = true    
      return if @skip <= 0
      @table.paginate({skip:@skip-1})

    @next = (e)=>
      #console.log('next')
      e.preventUpdate = true      
      return if @skip >= @pages - 1
      @table.paginate({skip:@skip+1})
      
    @goto = (e)=>
      e.preventUpdate = true      
