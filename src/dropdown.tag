rs-dropdown
  <yield />

  style(type='text/stylus').
    rs-dropdown      
      display:none
      &.open
        display:block
        position:relative

  script(type="text/coffee").

    @on 'mount', ->
      @hidden = true
      @open() if opts.open
      @root.addEventListener('click', @toggle)

    @on 'unmount', ->
      @root.removeEventListener('click', @toggle)
      
    @toggle = =>
      if @hidden then @open() else @close() 

    @open = =>
      @root.classList.add('open')
      @hidden = false
      @update()
      
    @close = => 
      @root.classList.remove('open')
      @hidden = true
      @update()