rs-panel
  .panel.panel-default
    .panel-heading.accordion-toggle(onclick="{toggle}")
      <yield from="header" />
    .panel-collapse(class="{'collapse':hidden,'in':shown,'collapsing':sliding}" style="{style}")
      .panel-body
        <yield />

  style(type="text/stylus").
    .accordion-toggle
      cursor: pointer

  script(type="text/coffee").
    @sliding = false

    @on 'mount', ->
      @root.trigger = @trigger
      @visible = true
      @hidden = true
      @shown = true
      @update()
      setTimeout =>
        @initial_height = @root.querySelector('.panel .panel-collapse').offsetHeight
        if opts.isOpen then @open() else @close()
      , 1000
      

    @on 'open', -> @open()
    @on 'close', -> @close()
    @on 'toggle', -> @toggle()

    @toggle = ->
      if @visible and !@sliding then @close() else @open()

    @open = -> 
      clearTimeout(@timeout) if @timeout?
      @timeout = null if @timeout?
      
      @visible = true
      @hidden = false
      @shown = false
      @sliding = true
      @update()

      @timeout = setTimeout =>
        @style = 'height:'+@initial_height+'px'
        @update()
        @timeout = setTimeout =>
          @style = {}
          @sliding = false
          @hidden = true       
          @shown = true
          @update()
        , 350
      , 20

    @close = ->    
      clearTimeout(@timeout) if @timeout?
      @timeout = null if @timeout?

      @hidden = false 
      @shown = false
      @sliding = true
      @update()

      @timeout = setTimeout =>
        @style = 'height:0'      
        @update()
        @timeout = setTimeout =>

          @sliding = false
          @hidden = true
          @visible = false
          @update()
        , 350
      , 0