rs-modal
  .modal.fade(if="{visible}",class="{in:slide}")
    .modal-dialog(class="{modal-wide:opts.wide}")
      .modal-content
        .modal-header
          button.close(onclick='{close}') ×
          <yield from="header" />
        .modal-body
          <yield />
        .modal-footer
          <yield from="footer" />
  .modal-backdrop.fade(show='{visible}',class='{in:slide}')
  
  style(type="text/stylus").
    rs-modal
      .modal
        display: block
        overflow-y: auto
        .modal-body
          //- max-height: 600px
        .modal-wide
          width: 80%
        
  script(type="text/coffee").    
    @escape = (ev)=> 
      @close() if ev.which == 27
      
    @on 'open', -> @open()
    @on 'close', -> @close()

    @open = ->
      document.addEventListener 'keydown', @escape
      @visible = true
      @update()
      setTimeout =>
        @slide = true
        @update()
      , 250

    @close = ->
      document.removeEventListener 'keydown', @escape
      @slide = false
      @update()
      setTimeout =>
        @visible = false
        @update()

        opts.onClose() if opts.onClose?
      , 250