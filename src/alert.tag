rs-alert
  .alert.alert-dismissible.fade(role="alert", class="{'in':show}", class="alert-{opts.type}", style="transition: opacity {transitionLength}s linear")
    button.close(type="button", data-dismiss="alert", aria-label="Close" onclick="{close}")
      span(aria-hidden="true") &times;
    <yield/>

  script(type="text/coffeescript").
    @show = true
    @transitionLength = opts.transitionLength || "0.2"

    @on 'mount', ->
      @timeout = (setTimeout (=> @close()), opts.timeout) if opts.timeout
      @root.querySelector('.alert').addEventListener('transitionend', @closed)

    @closed = ()=>
      clearTimeout(@timeout) if @timeout
      @opts.onclose() if opts.onclose?
      @unmount()

    @close = ()=>
      @show = false
      @update()