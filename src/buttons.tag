rs-button
  button.btn(onclick="{click}",class="{opts.rsClass} {'disabled':sending || failure || successful, 'btn-danger':failure, 'btn-success':successful}")
    <yield/>
    span(show="{sending || successful || failure}") &nbsp;
    i.fa.fa-spinner.fa-pulse(show="{sending}")
    i.fa.fa-times(show="{failure}")
    i.fa.fa-check(show="{successful}")

  script(type="text/coffee").

    @on 'mount', ->
      @timeout = opts.timeout || 1000
      
    @click = ()->
      if opts.onClick? and !@sending and !@successful and !@failure
        @sending = true
        @update()

        @opts.onClick().then ()=>
          @sending = false
          @successful = true
          @update()

          setTimeout =>
            @successful = false
            @update()
          , @timeout

        .catch (err)=>
          @sending = false
          @failure = true
          @update()

          setTimeout =>
            @failure = false
            @update()
          , @timeout