// pagination
//   ul.pagination(if="{pages.length > 1}")
//     li.prev(class="{'disabled':currentpage==0}"): a(onclick="{prev}") &laquo;
//     li(each="{page in pages}",class="{'active':currentpage == page}"): a(onclick="{setPage.bind(null,page)}") {page+1}
//     li.next(class="{'disabled':currentpage+2>total}"): a(onclick="{next}") &raquo;

//   script(type='text/coffeescript').

//     @on 'mount', ->
//       @perpage = +opts.perpage || 10
//       @max =  +opts.max || 10 
//       @pages = []
//       @currentpage = 0
//       @data = []

//       @setPage()

//     @next = =>
//       if @currentpage < @total-1
//         @currentpage++
//         @setPage()

//     @prev = =>
//       if @currentpage > 0
//         @currentpage--
//         @setPage()

//     @setPage = (pagenum)=>
//       @currentpage = pagenum if pagenum?

//       if opts?.data?.length?
//         @total = Math.ceil(opts.data.length / @perpage)
//         @start = @perpage * @currentpage
//         @end = @start + @perpage
//         @end = opts.data.length if opts.data && @end > opts.data.length
//         @start = 0 if @start < 0 || @start > opts.data.length
//         @data = opts.data.slice(@start,@end) 
//       else
//         @total = 0

//       if(@currentpage > (@max / 2) && @max < @total)
//         @first = @currentpage - @max / 2
//       else
//         @first = 0
//       if(@first + @max > @total)
//         @first = @total - @max
//         @first = 0 if  @first < 0

//       @pages = [@first...@total] if @data
//       opts.updatepages(@data) if opts.updatepages?

//       @update()
