rs-tabs
  ul.nav.nav-tabs
    li(each="{tab, i in tabs}", class="{'active': i == activeTab}", onclick="{openTab.bind(null, tab, i)}") 
      a {tab.heading}

  .tab-content
    <yield />

  style(type="text/stylus").
    rs-tabs
      ul
        li
          cursor: pointer

  script(type="text/coffee").
    @activeTab = 0

    @on 'before-mount', ->
      @root.trigger = @trigger
      @tabs = @tags['rs-tab']
      @tabs = [@tabs] if(!Array.isArray(@tabs))
      @tabs.map (t, idx)-> 
        @activeTab = idx if t.active
      @update()

    @changeTab = (tabId)=>
      @activeTab = tabId
      @tabs.map (t, i)-> 
        if i == tabId then t.open() else t.close()
      @update()
    
    @openTab = (tab, idx)=>
      if @activeTab != idx
        @activeTab = idx
        @opts.ontabchange(idx) if @opts.ontabchange?
        @tabs.map (t)-> t.close() if t.active
        setTimeout =>
          tab.open() if tab?
          @update()
        , 100
        @update()

rs-tab
  .tab.tab-pane.fade(show="{active}",class="{'in': slide}")
    <yield />
  
  script(type="text/coffee").
    @active = opts.active
    @heading = opts.heading || ''

    @on 'mount', ->
      @open() if opts.active

    @open = ->
      @active = true
      @update()
      setTimeout =>
        @slide = true
        @update()
      , 100
      
    @close = ->
      @slide = false
      @update()
      setTimeout =>
        @active = false
        @update()
      , 100