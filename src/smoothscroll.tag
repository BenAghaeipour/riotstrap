smoothscroll(onclick="{handleClick}")
  <yield/>

  style(type='text/stylus').
    smoothscroll
      display: block

  script(type="text/coffee").
    @utils = require '../shared/utils.coffee'

    @handleClick = (e)=>
      @offset = +opts.offset || 0
      @duration = Math.round opts.duration
      @start_time = Date.now()
      @end_time = @start_time + @duration
      @target = document.querySelector(opts.to)
      @distance = Math.round (@utils.getOffset(@target).top - @utils.getScroll().top + @offset)
      @scroll()
      
    smooth_step = (start, end, point) ->
      return 0 if point <= start
      return 1 if point >= end
      x = (point - start) / (end - start)
      x * x * (3 - (2 * x))

    @scroll = ()=>
      @previous_top = @utils.getScroll().top
      @previous_delta = 0
      new Promise (resolve, reject)=>
        @scroll_frame(resolve, reject)

    @scroll_frame = (resolve, reject)=>
      return reject('interrupted') if (@utils.getScroll().top ) != @previous_top
      now = Date.now()
      return resolve('times up') if now >= @end_time      
      delta = Math.round smooth_step(@start_time, @end_time, now) * @distance
      change = Math.round(delta - @previous_delta)
      window.scrollBy 0, change
      return resolve('finished') if @utils.getScroll().top == @previous_top and @utils.getScroll().top  == @utils.getOffset(@target).top + @offset

      @previous_delta = delta
      @previous_top = @utils.getScroll().top 

      setTimeout =>
        @scroll_frame(resolve, reject)
      , 0